var express = require('express');
var router = express.Router();

var codegen_controller = require('../controllers/codegenController');

/* Sample Post call. */
router.post('/generate', codegen_controller.codegen_generate_post);

/* GET users listing. */
router.get('/generate', codegen_controller.codegen_generate_get);

module.exports = router;
