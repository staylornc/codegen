const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

// in the sample, this is a model from a DB - mongoose
class BlockCode {
    constructor(name) {
        this.name = name;
    }
}

exports.codegen_generate_get = function(req, res, next) {     
    res.render('codegen_form', { title: 'Generate Lua' });
  };

/* Sample Post call. */
exports.codegen_generate_post = [
    // Validated the blockcode field is not empty.
   body('name', 'Block code required').isLength({min: 1 }).trim(), 

    // Sanitize (trim and escape) the blockcode field.
    sanitizeBody('name').trim().escape(),

    // Process request after validation and sanitization.
    (req, res, next) => {

        // Extract the validation errors from a request.
        const errors = validationResult(req);

        // Create a blockcode object with escaped and trimmed data.
        var blockCode = new BlockCode(
            {name: req.body.name }
        );

        if (!errors.isEmpty()) {
            // There are errors. Render the form again with sanitized values/error messages.
            res.render('codegen_form', { title: 'Generate Lua', blockCode: blockCode, errors: errors.array()});
          return;
        }
        else {
            // Data from form is valid.
            // TODO: What to do here?
            res.render('codegen_form', { title: 'Lua generated', blockCode: blockCode, errors: errors.array()});
                //    genre.save(function (err) {
                //      if (err) { return next(err); }
                //      // Genre saved. Redirect to genre detail page.
                //      res.redirect(genre.url);
                //    });
        }
    }
];


// function(req, res) {
//     res.send('NOT IMPLEMENTED: CodeGen Generator POST');
// };